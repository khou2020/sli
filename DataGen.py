import sys, os, subprocess

dir = sys.argv[1]
with open(sys.argv[2], 'w') as fout:
	for f in os.listdir(dir):
		fname = dir + '/' + f
		if os.path.isfile(fname) and f[-4:] == '.txt':
			with open(fname) as fin:
				for line in fin:
					fout.write(line)

	