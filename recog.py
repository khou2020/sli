import sys, os, subprocess

dir = sys.argv[1]
for f in range(int(sys.argv[2]), int(sys.argv[3])):
	fname = dir + '/' + str(f) + '.wav'
	if os.path.isfile(fname):
		ret = subprocess.call('.\\Data\\pocketsphinx\\pocketsphinx_continuous.exe -infile ' + fname + ' -hmm Data/pocketsphinx/model/en-us/en-us -allphone phonetic.lm -backtrace yes -beam 1e-20 -pbeam 1e-20 -lw 2.0 > ' + fname[:-4] + '.txt', shell = True)
		#if ret != 0:
			#os.remove(fname[:-4] + '.txt')
	else:
		break

	