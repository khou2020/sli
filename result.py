import sys, os

def getret(fname:str):
	src = []
	with open(fname) as fin:
		for line in fin:
			tokens = line.split()
			if len(tokens) == 0 or tokens[0] == 'SIL':
				continue
			for i in range(len(tokens)):
				if tokens[i] == 'logprob=':
					src.append(float(tokens[i + 1]))
					break;
	return src

def main(argc:int, argv:list):
	enen = getret('Data/enonen.txt')
	ench = getret('Data/enonch.txt')
	chen = getret('Data/chonen.txt')
	chch = getret('Data/chonch.txt')

	ee = 0
	ec = 0
	for i in range(len(enen) - 1):
		if enen[i] > chen[i]:
			ee += 1
		else:
			ec += 1

	cc = 0
	ce = 0
	for i in range(len(chch) - 1):
		if chch[i] > ench[i]:
			cc += 1
		else:
			ce += 1

	print('English: ', ee / (ee + ec), ' (', ee, ':', ec, ')')
	print('Chinese: ', cc / (cc + ce), ' (', cc, ':', ce, ')')

if __name__ == '__main__':
	main(len(sys.argv), sys.argv)